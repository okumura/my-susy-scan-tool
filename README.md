# 動作確認メモ
2022.6.30にlxplusで動作を確認しました。その時に若干の修正も加えました。

# To resume the work with the installed directory
- cd workspace/my-susy-scan-tool
- source setup.sh

# To get started
## Set environment
- cd workspace # where you have enough space do not use EOS for the workspace as of October, 2019. 
- git clone ssh://git@gitlab.cern.ch:7999/okumura/my-susy-scan-tool.git
- cd my-susy-scan-tool
- export SUSYSCANBASE=\`pwd\`
- python3 -m venv python3
## install PyMultiNest
- source python3/bin/activate
- git clone https://github.com/JohannesBuchner/PyMultiNest/
- cd PyMultiNest
- python setup.py install
- cd ../
## install MultiNest
- git clone https://github.com/JohannesBuchner/MultiNest
- cd MultiNest/build
- cmake3 ..
- \# cmake3 -DBLAS_LIBRARIES=/usr/lib64/libblas.so.3 -DLAPACK_LIBRARIES=/usr/lib64/liblapack.so.3 .. \# pcatut23, 24 ではこちらのコマンドを実行のこと
- make
- cd ../../
## install cuba
- git clone https://github.com/JohannesBuchner/cuba/
- cd cuba
- ./configure
- ./makesharedlib.sh
- cd ../
## install some additional modules
- pip install --upgrade pip
- pip install numpy
- pip install scipy
- pip install matplotlib
## PyMultiNest import test
- export LD_LIBRARY_PATH=${SUSYSCANBASE}/MultiNest/lib:${SUSYSCANBASE}/cuba:$LD_LIBRARY_PATH
- python -c 'import pymultinest'
- python -c 'import pycuba'
## install SPheano
- wget https://spheno.hepforge.org/downloads/?f=SPheno-4.0.4.tar.gz
- mv index.html\?f\=SPheno-4.0.4.tar.gz SPheno-4.0.4.tar.gz
- tar -xzvf SPheno-4.0.4.tar.gz 
- cd SPheno-4.0.4/
- emacs -nw Makefile # change the compiler definition from ifort to gfortran
- make
- cd ../
- export PATH=${SUSYSCANBASE}/SPheno-4.0.4/bin/:$PATH
## install Micromegas
- wget --no-check-certificate https://lapth.cnrs.fr/micromegas/downloadarea/code/micromegas_5.0.8.tgz
- tar -xzvf micromegas_5.0.8.tgz
- cd micromegas_5.0.8/
- gmake
- cd MSSM
- emacs -nw main.cpp # to comment out the following points (1) to run using SLHA file (2) to run only OMEGA
   - `//#define EWSB`
   - `//#define MASSES_INFO`
   - `//#define CONSTRAINTS`
   - `//#define INDIRECT_DETECTION`
   - (直接探索の結果も含めたLikelihoodを計算をするように修正をしたので、この行はコメントアウトしない `//#define CDM_NUCLEON` )
- gmake main=main.cpp
- mv main micromegasMSSM
- cd ../../
- export PATH=${SUSYSCANBASE}/micromegas_5.0.8/MSSM:$PATH
## install prospino2
- git clone ssh://git@gitlab.cern.ch:7999/okumura/my-prospino2-codes.git
- cd my-prospino2-codes
- make
- cd ../
- export PATH=${SUSYSCANBASE}/my-prospino2-codes/:$PATH
## Run
- cd test
- mkdir tmp
- cat run_user_okumura.sh # please check what commands are used 
- sh run_user_okumura.sh

## (参考) install pymultinest-tutorial
- cd ../
- git clone https://github.com/JohannesBuchner/pymultinest-tutorial
- mkdir tutorial
- cd tutorial/
- cp ../pymultinest-tutorial/example1/1d_multimodal.py ./
- mkdir out
- python 1d_multimodal.py
- multinest_marginals.py out/


# Scan する / Likelihood で constraint をかけるパラメタを減らしてみる
- 例えば myparameters.py を以下のように修正する。 parameters に append されないパラメタは、scan 結果の plot からも落ちるし、 Likelihoood の計算からも外れるように書いた。
```
doSPheno=True
doMicrOMEGAs=False # disabled

# default 9 parameters
parameters=["m0", "m12", "a0", "tanbeta", "signmu", "mb", "mt", "alphas", "invalpha"]
# additional parameters (to be used in constraints)
if doSPheno:
        parameters.append("mw")
        parameters.append("mh")
        parameters.append("mchi10")
        parameters.append("mchi1+")
        parameters.append("deltam")
        # parameters.append("gm2") # commented-out
        # parameters.append("bsg") # commented-out
        # parameters.append("bsmm") # commented-out
        # parameters.append("bsnn") # commented-out
        # parameters.append("Bd2mm") # commented-out
        # parameters.append("Bd2tt") # commented-out
        # parameters.append("Bs2mm") # commented-out
        # parameters.append("Bs2tt") # commented-out
if doMicrOMEGAs:
        parameters.append("relicdensity")

```

# 走っているコード
- MultiNest: ベイジアン推定をつかって、Prior + Likelihood の制約のもとで、MCMC とかで取りうるモデル点を出力するような仕掛け。
   - MultiNest 中のパラメタは、SUSY free parameters と、 SM constants, あと depending parameters (そのほかの定数から計算されるもの) に分けられる
   - Likelihood の確率分布に従った parameter セットを出力してくれる
   - 各パラメタセットに対して Likelihood を計算するが、それは、 depending parameters の値とその constraints による。
- SPheno: SUSY free parameters と model から、SUSY の mass spectrum や、Low energy Observables を計算してくれる。MultiNest 中ではこれを Depending parameters として使っている。
- micrOMEGAs: SPheno の output (SPheno.spc) から、Dark Matter 関連の Observables (relic density や、Spin Independent Nuclear-DM cross section 等) を計算する。これも Depending parameters として使われる。

## 試しに、各々のコードを走らせてみよう
- cd my-susy-scan-tool/test/input
   - MultiNest 中でスキャン時に `prepSLHA` 関数で作られたとある LesHouches.in ファイルが準備してあります。
- SPheno LesHouches.in # SPheno.spc ができる
- micromegasMSSM SPheno.spc # relic density を計算する. # 標準出力に出される xf が Freeze out temperature, Omega が relic density.
- ln -s SPheno.spc prospino.in.les_houches; ln -s ${SUSYSCANBASE}/my-prospino2-codes/Pro2_subroutines/ .; prospino_2_1_1.run # ニュートラリーノ対生成


## output を ROOT tuple 化する
- 準備 (これは SPheno を走らせる作業なので同じく python3 環境下で)
   - 出てきた N events に対して SPheno を走らせるコマンド
   - cd my-susy-scan-tool/test/
   - python analysis.py --params out/params.json --points out/.txt --output outtmp --skip_prospino
   - outtmp.json が完成
- ROOT ファイル作成 (こちらは全く違う環境で root を準備して行う)
   - 新しく綺麗な terminal を開く
   - root のセットアップ (setupATLAS && lsetup "root 6.18.04-x86_64-centos7-gcc8-opt")
   - 上の手続きで作った outtmp.json を使って最終的な ntuple を作る
   - python ntuplizer.py --json outtmp.json --output outputrootfile
   - outputrootfile.root が完成

## output を ROOT tuple 化する + Prospino2 の出力を含む
- 準備 (これは SPheno を走らせる作業なので同じく python3 環境下で)
   - 出てきた N events に対して SPheno と Prospino を走らせる。 1 event あたり 7 分 30 秒かかるので並列に走らせる。
   - sh prepare_prospino.sh tmp1
   - cd tmp1
   - python ../analysis.py --params ../out/params.json --points ../out/.txt --output outtmp --ndivs 100 --nth 1 # first 1%
   - cd ../
   - 
   - sh prepare_prospino.sh tmp2
   - cd tmp2
   - python ../analysis.py --params ../out/params.json --points ../out/.txt --output outtmp --ndivs 100 --nth 2 # second 1%
   - cd ../
   - 
   - sh prepare_prospino.sh tmp3
   - cd tmp3
   - python ../analysis.py --params ../out/params.json --points ../out/.txt --output outtmp --ndivs 100 --nth 3 # third 1%
   - cd ../
   - 
   - ...
   - 100 分割すべて回す
   - ndivs の値を大きくすると分割が増える。
- ROOT ファイル作成 (こちらは全く違う環境で root を準備して行う)
   - 並列で走らせた output.json を全て読ませる必要がある。
   - 以下のようなファイル名が1行に一つならんだテキストファイル (input.txt) を作る
      - tmp1/outtmp.json
      - tmp2/outtmp.json 
      - tmp3/outtmp.json
      - tmp4/outtmp.json
      - ..
      - tmp100/outtmp.json
   -  python ntuplizer.py --list_file input.txt
      - lo_xsec_1_1 : LO cross section of neutralino neutralino pair production
      - lo_xsec_1_5 : LO cross section of neutralino chargino (positive) pair production
      - lo_xsec_1_7 : LO cross section of neutralino chargino (negative) pair production
      - l0_xsec_5_7 : LO cross section of chargino pair production
      - and their relative errors
      - NLO cross sections in the same manner
      - all cross sections in pb

