#pdfname='CT10nlo'
#pdfname='MMHT2014lo68cl'
#pdfname='MMHT2014nlo68cl'
#pdfname='MMHT2014nnlo68cl'
if [ $# -ne 1 ]; then
    echo "sh $0 <pdfname>"
    echo "sh $0 CT10nlo"
    echo "sh $0 MMHT2014lo68cl"
    echo "sh $0 MMHT2014nlo68cl"
    echo "sh $0 MMHT2014nnlo68cl"
else
    echo "wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/${pdfname}.tar.gz -O- | tar xz -C ${SUSYSCANBASE}/lhapdf/share/LHAPDF"
    wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/${pdfname}.tar.gz -O- | tar xz -C ${SUSYSCANBASE}/lhapdf/share/LHAPDF
fi
