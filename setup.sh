source python3/bin/activate
export SUSYSCANBASE=`pwd`
export LD_LIBRARY_PATH=${SUSYSCANBASE}/MultiNest/lib:${SUSYSCANBASE}/cuba/:$LD_LIBRARY_PATH
export PATH=${SUSYSCANBASE}/SPheno-4.0.4/bin/:$PATH
export PATH=${SUSYSCANBASE}/micromegas_5.0.8/MSSM:$PATH
export PATH=${SUSYSCANBASE}/my-prospino2-codes/:$PATH
module load mpi/mpich-x86_64
export PATH=${SUSYSCANBASE}/lhapdf/bin/:$PATH
export PATH=${SUSYSCANBASE}/SusHi-1.7.0/bin:$PATH
export LD_LIBRARY_PATH=${SUSYSCANBASE}/lhapdf/lib/:${LD_LIBRARY_PATH}
