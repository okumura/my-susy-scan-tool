##########################################
# number of dimensions our problem has
# global parameters
##########################################
doSPheno=True
doMicrOMEGAs=True
flatProbabilityForTesting=False # only for testing purpose

# default 8 parameters
parameters=["m0", "m32", "tanbeta", "signmu", "mb", "mt", "alphas", "invalpha"]
# additional parameters (to be used in constraints)
if doSPheno:
         parameters.append("mw")
         parameters.append("mh")
         parameters.append("mgluino")
         parameters.append("mchi10")
         parameters.append("mchi1+")
         parameters.append("deltam")
         parameters.append("gm2")
         parameters.append("bsg")
         parameters.append("bsmm")
         parameters.append("bsnn")
#        parameters.append("Bd2mm")
#        parameters.append("Bd2tt")
#        parameters.append("Bs2mm")
#        parameters.append("Bs2tt")
if doMicrOMEGAs:
         parameters.append("relicdensity")
SUSYModel=1 # SUSY model 1=sugra (to be updated in susyscan depending on the argument)

nameIndexMap={}
indexNameMap={}
ii=0
for name in parameters:
        indexNameMap.update({ii:name})
        nameIndexMap.update({name:ii})
        ii=ii+1
n_params = len(parameters)
dryRunForDevelopment=True
##########################################


def GetParamIndex(name):
        if not (name in nameIndexMap.keys()):
                print ('WARNING name is not defined %s'%(name))
                return 0
        return nameIndexMap[name]

def GetParamName(index):
        if not (index in indexNameMap.keys()):
                print ('WARNING name is not defined %s'%(index))
                return 0
        return indexNameMap[index]

def GetParamFromName(cube, name):
        if not (name in nameIndexMap.keys()):
                print ('WARNING name is not defined %s'%(name))
                return 0
        return cube[nameIndexMap[name]]

def IsRegistered(name):
        if not (name in nameIndexMap.keys()): 
                return False
        else: return True
