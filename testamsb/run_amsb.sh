if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    rm -rf out
    mkdir out
    time python susyscan.py --model 3
    time multinest_marginals.py out/
    mkdir /eos/home-k/ksugizak/www/amsb/amsb${1}
    \cp out/marg.pdf out/params.json out/phys_live.points /eos/home-k/ksugizak/www/amsb/amsb${1}/
    echo "https://cernbox.cern.ch/index.php/apps/files/?dir=/www&"

fi
