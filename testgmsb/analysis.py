import json
import re
import argparse
import subprocess
import susyscan

def getParameterNames(json_file='params.json', debug=False):
    jsonFile = open(json_file)
    parameters = json.load(jsonFile)
    
    if debug:
        for parameter in parameters:
            print(parameter)
            
    return parameters
    
def GetSPhenoResults_mSUGRA(point):
    prepSLHA_mSUGRA(point)
    command = ['SPheno', 'LesHouches.in']
    subprocess.run(command)
    results=susyscan.readSLHA()
    return results

def GetSampledPoints(names, file_name='phys_live.points', debug=False):
    points=[]
    point_file=open(file_name)
    lines=point_file.readlines()
    for line in lines:
        #print(line.replace('\n', ''))
        components=re.split(' +', line.replace('\n', ''))
        point={}
        values=[]
        for ii, comp in enumerate(components):
            if comp=='': continue
            values.append(float(comp))
        for ii, name in enumerate(names):
            value=values[ii]
            point[name]=value
        points.append(point)
    return points

def protection(names, debug=False):
    ii=0
    for name in names:
        names[ii]=name.replace('+', 'charged')
        ii=ii+1

def prepSLHA_mSUGRA(point, MZ=9.11876000e+01):
    f = open('LesHouches.in', 'w')
    print ('Block MODSEL                    # Select model', file=f)
    print (' 1   1                          # sugra', file=f)
    print ('Block SMINPUTS                  # Standard Model inputs', file=f)
    print (' 1   %f                         # alpha^(-1) SM MSbar(MZ)'%(point['invalpha']), file=f)
    print (' 2   1.16637000e-05             # G_Fermi', file=f)
    print (' 3   %f                         # alpha_s(MZ) SM MSbar'%(point['alphas']), file=f)
    print (' 4   %f                         # MZ(pole)'%(MZ), file=f)
    print (' 5   %f                         # mb(mb) SM MSbar'%(point['mb']), file=f)
    print (' 6   %f                         # mtop(pole)'%(point['mt']), file=f)
    print (' 7   1.77700000e+00             # mtau(pole)', file=f)
    print ('Block MINPAR    # Input parameters', file=f)
    print (' 1   %f                         # m0'%(point['m0']), file=f)
    print (' 2   %f                         # m12'%(point['m12']), file=f)
    print (' 3   %f                         # tanb'%(point['tanbeta']), file=f)
    print (' 4   %f                         # sign(mu)'%(point['signmu']), file=f)
    print (' 5   %f                         # A0'%(point['a0']), file=f)
    print ('Block SPhenoInput       # SPheno specific input', file=f)
    print (' 1  -1                  # error level', file=f)
    print (' 2   1                  # SPA conventions', file=f)
    print ('11   1                  # calculate branching ratios', file=f)
    print ('12   1.00000000E-04     # write only branching ratios larger than this value', file=f)
    print ('21   0                  # calculate cross section', file=f)
    
def main(output_file='output', points_file='phys_live.points', params_file='params.json', debug=False):
    names=getParameterNames(json_file=params_file, debug=debug)
    protection(names)
    names.append('likelihood')
    points=GetSampledPoints(names=names, file_name=points_file, debug=debug)
    
    results_list=[]
    for ii, point in enumerate(points):
        results=GetSPhenoResults_mSUGRA(point)
        if len(results)==0:
            print('WARNING: SPheno does have problem')
            continue        
        for param_name in point.keys():
            results[param_name]=point[param_name]
        results_list.append(results)
        
    f=open('%s.json'%(output_file), 'w')
    json.dump(results_list, f)
    
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser("usage: ntuplizer.py")
    parser.add_argument("--output", help='e.g. "output" to create output.root', action="store", default="output")
    parser.add_argument("--params", help='file path for params.json', action="store", default="params.json")
    parser.add_argument("--points", help='file path for phys_live.points', action="store", default="phys_live.points")
    parser.add_argument("--debug", help='debug', action="store_true", default=False)
    argments = parser.parse_args()
    
    main(output_file=argments.output,
         points_file=argments.points, 
         params_file=argments.params, 
         debug=argments.debug)
