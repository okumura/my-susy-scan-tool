import json
import re
import argparse
import subprocess
import susyscan

def getParameterNames(json_file='params.json', debug=False):
    jsonFile = open(json_file)
    parameters = json.load(jsonFile)
    
    if debug:
        for parameter in parameters:
            print(parameter)
            
    return parameters

def ReadProspinoOutput():
    res={}
    prospino_dat = open('prospino.dat')
    lines=prospino_dat.readlines()
    if len(lines) != 3: return res
    components0=re.split(' +', lines[0].replace('\n', ''))
    #components2=re.split(' +', lines[2].replace('\n', ''))
    #print (components0)
    #print (components2)
    res['lo_xsec']=components0[9]
    res['lo_xsec_rel_error']=components0[10]
    res['nlo_xsec']=components0[11]
    res['nlo_xsec_rel_error']=components0[12]
    return res
    
def RunProspino(main_function_name, prefix, results):
    command = [main_function_name]
    subprocess.run(command)    
    parameters = ReadProspinoOutput()
    for param_name in parameters.keys():
        results[param_name+prefix]=float(parameters[param_name])
    
    
def GetSPhenoResults_mSUGRA(point):
    prepSLHA_mSUGRA(point)
    command = ['SPheno', 'LesHouches.in']
    subprocess.run(command)
    results=susyscan.readSLHA()
    return results
    
def GetSampledPoints(names, file_name='phys_live.points', debug=False):
    points=[]
    point_file=open(file_name)
    lines=point_file.readlines()
    for line in lines:
        if debug: print(line.replace('\n', ''))
        components=re.split(' +', line.replace('\n', ''))
        point={}
        values=[]
        for ii, comp in enumerate(components):
            if comp=='': continue
            values.append(float(comp))
        for ii, name in enumerate(names):
            value=values[ii]
            point[name]=value
        points.append(point)
    return points

def protection(names, debug=False):
    ii=0
    for name in names:
        names[ii]=name.replace('+', 'charged')
        ii=ii+1

def prepSLHA_mSUGRA(point, MZ=9.11876000e+01):
    f = open('LesHouches.in', 'w')
    print ('Block MODSEL                    # Select model', file=f)
    print (' 1   1                          # sugra', file=f)
    print ('Block SMINPUTS                  # Standard Model inputs', file=f)
    print (' 1   %f                         # alpha^(-1) SM MSbar(MZ)'%(point['invalpha']), file=f)
    print (' 2   1.16637000e-05             # G_Fermi', file=f)
    print (' 3   %f                         # alpha_s(MZ) SM MSbar'%(point['alphas']), file=f)
    print (' 4   %f                         # MZ(pole)'%(MZ), file=f)
    print (' 5   %f                         # mb(mb) SM MSbar'%(point['mb']), file=f)
    print (' 6   %f                         # mtop(pole)'%(point['mt']), file=f)
    print (' 7   1.77700000e+00             # mtau(pole)', file=f)
    print ('Block MINPAR    # Input parameters', file=f)
    print (' 1   %f                         # m0'%(point['m0']), file=f)
    print (' 2   %f                         # m12'%(point['m12']), file=f)
    print (' 3   %f                         # tanb'%(point['tanbeta']), file=f)
    print (' 4   %f                         # sign(mu)'%(point['signmu']), file=f)
    print (' 5   %f                         # A0'%(point['a0']), file=f)
    print ('Block SPhenoInput       # SPheno specific input', file=f)
    print (' 1  -1                  # error level', file=f)
    print (' 2   1                  # SPA conventions', file=f)
    print ('11   1                  # calculate branching ratios', file=f)
    print ('12   1.00000000E-04     # write only branching ratios larger than this value', file=f)
    print ('21   0                  # calculate cross section', file=f)
    
def main(output_file='output', points_file='phys_live.points', params_file='params.json', 
         ndivs=1, nth=1, skip_prospino=False, debug=False):
    names_tmp=getParameterNames(json_file=params_file, debug=debug)
    protection(names_tmp)
    names=[]
    names.append('probability')
    names.append('likelihood')
    for name in names_tmp: names.append(name)
    points=GetSampledPoints(names=names, file_name=points_file, debug=debug)
    if debug: print('%d points retrieved'%len(points))
    
    results_list=[]
    total=len(points)
    for ii, point in enumerate(points):
        if not (ii*ndivs >= total*(nth-1)) : 
            if debug: print ('ii=%d to be skipped'%(ii))
            continue
        if not (ii*ndivs < total*nth): 
            if debug: print ('ii=%d to be skipped'%(ii))
            continue
        if debug: print ('ii=%d to be stored'%(ii))
        
        results=GetSPhenoResults_mSUGRA(point)
        if len(results)==0:
            print('WARNING: SPheno does have problem')
            continue        
        for param_name in point.keys():
            results[param_name]=point[param_name]
            
        if (not skip_prospino):
            RunProspino('prospino_2_1_1.run', '_1_1', results)
            RunProspino('prospino_2_1_5.run', '_1_5', results)
            RunProspino('prospino_2_1_7.run', '_1_7', results)
            RunProspino('prospino_2_5_7.run', '_5_7', results)
            RunProspino('prospino_2_1_2.run', '_1_2', results)
            RunProspino('prospino_2_2_2.run', '_2_2', results)
            RunProspino('prospino_2_2_5.run', '_2_5', results)
            RunProspino('prospino_2_2_7.run', '_2_7', results)
        
        results_list.append(results)
        
    f=open('%s.json'%(output_file), 'w')
    json.dump(results_list, f)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser("usage: ntuplizer.py")
    parser.add_argument("--output", help='e.g. "output" to create output.root', action="store", default="output")
    parser.add_argument("--params", help='file path for params.json', action="store", default="params.json")
    parser.add_argument("--points", help='file path for phys_live.points', action="store", default="phys_live.points")
    parser.add_argument("--ndivs", help='how many blocks in total', action="store", default=1, type=int)
    parser.add_argument("--nth", help='which of ndvis', action="store", default=1, type=int)
    parser.add_argument("--skip_prospino", help='skip_prospino', action="store_true", default=False)
    parser.add_argument("--debug", help='debug', action="store_true", default=False)
    argments = parser.parse_args()
    
    main(output_file=argments.output,
         points_file=argments.points, 
         params_file=argments.params, 
         ndivs=argments.ndivs, 
         nth=argments.nth,
         skip_prospino=argments.skip_prospino,
         debug=argments.debug)
