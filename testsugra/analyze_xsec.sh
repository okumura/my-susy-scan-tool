#!/bin/bash

id=${1}

echo ${id}
cd /data/data10/zp/ksugizak/workspace/my-susy-scan-tool/
source setup.sh
cd testsugra128/tmp${1}
python ../analysis.py --params ../out/params.json --points ../out/.txt --output outtmp --ndivs 200 --nth ${1}
cd ../

