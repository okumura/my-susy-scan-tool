# https://root-forum.cern.ch/t/python-list-in-a-tree-branch/8391/5

import ROOT
import json
import re
import argparse
import array

class NtuplizerOk:
    def __init__(self, json_file, output_file, debug):
        self.m_json_file = json_file
        self.m_output_file = output_file
        self.m_debug = debug
        self.m_points = []
        self.m_point  = {}        
        jsonFile = open(json_file)
        parameters = json.load(jsonFile)
        
        self.decode(parameters=parameters)
        
    def decode(self, parameters):
        self.m_points = []        
        for param in parameters:
            self.m_point = {}
            self.decode_dict(prefix='', parameters=param)
            self.m_points.append(self.m_point)
            #if self.m_debug print (self.m_point)
            
            
    def decode_dict(self, prefix, parameters):
        for name in parameters.keys():
            param = parameters[name]
            new_name = prefix + '_' + name if prefix!='' else name
            if (type(param) == float) :                 
                self.m_point[new_name] = param
            elif (type(param) == str) :
                self.m_point[new_name] = float(param)
            elif (type(param) == dict) : 
                self.decode_dict(prefix=new_name, parameters=param)
            elif (type(param) == list) : 
                self.decode_list(prefix=new_name, parameters=param)
            else:
                print ('not defined type=',type(param))
                
    def decode_list(self, prefix, parameters):
        allFloat = True
        for param in parameters:
            if (type(param) != float) : 
                allFloat = False
                #print('cannot be ntuplize', param, type(param))
                break
        if allFloat:
            v = ROOT.std.vector( float )()
            for param in parameters:
                v.push_back(param)
            self.m_point[prefix] = param
                
    def make_TTree(self):
        f = ROOT.TFile('%s.root'%(self.m_output_file), 'RECREATE')
        t = ROOT.TTree('tree', 'tree')
        if len(self.m_points)==0:
            print('ERROR: please read the json file first')
        else:
            tree_dict = {}
            for name in self.m_points[0].keys():
                param=self.m_points[0][name]
                if type(param)==float:
                    arr = array.array('f', [0])
                    tree_dict[name]=arr
                    t.Branch(name, tree_dict[name], name+'/F')
                else:
                    print('NOT prepared yet type=', type(param))
                    
        for point in self.m_points:
            for name in point.keys():
                if (not name in tree_dict.keys()):
                    print('%s is not defined'%(name))
                else:
                    tree_dict[name][0]=point[name]
            t.Fill()
            
        t.Write()
        f.Write()
        f.Close()
        


    # def makeTTree(names, points, debug=False):
    #     structCode="struct MyData{\n"
    #     for name in names: structCode=structCode+"Float_t %s;\n"%(name)        
    #     structCode=structCode+"};"
    #     ROOT.gROOT.ProcessLine(structCode);
        
    #     branchNames=''
    #     ii=0
    #     for name in names: 
    #         ii=ii+1
    #         branchNames=branchNames+"%s/F"%(name)
    #         if ii<len(names): branchNames=branchNames+":"
        
    #     from ROOT import MyData
    #     myData = MyData()
        
    #     t = ROOT.TTree('tree', 'tree')
    #     t.Branch('myData', myData, branchNames)
        
    #     for point in points : 
    #         if debug: print(point)
    #         ii=0
    #         for value in point:
    #             if ii==len(names): break
    #             setattr(myData, names[ii], value)
    #             ii=ii+1
    #         t.Fill()
        
    #     return t
    
    
def main(output_file='output', json_file='output_analysis.json', debug=False):
    np = NtuplizerOk(json_file=json_file, output_file=output_file, debug=debug)
    np.make_TTree()
    # for parameter in parameters:
    #     for key in sorted(parameter.keys()):
    #         print(type(parameter[key]))
    # tree=makeTTree(names=names, points=points, debug=debug)
    
    # # writing file
    # f = ROOT.TFile('%s.root'%(output_file), 'RECREATE')
    # tree.Write()
    # f.Write()
    # f.Close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser("usage: ntuplizer.py")
    parser.add_argument("--output", help='e.g. "output" to create output.root', action="store", default="output")
    parser.add_argument("--json", help='file path for output_analysis.json', action="store", default="output_analysis.json")
    parser.add_argument("--debug", help='debug', action="store_true", default=False)
    argments = parser.parse_args()
    
    main(output_file=argments.output,
         json_file=argments.json, 
         debug=argments.debug)
    
