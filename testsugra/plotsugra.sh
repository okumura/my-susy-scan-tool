if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    root -l -b -q 'plot.C("outputrootfile_post.root")'
    \cp -f output.pdf /eos/home-k/ksugizak/www/sugra/sugra${1}/sugra${1}root.pdf
fi
