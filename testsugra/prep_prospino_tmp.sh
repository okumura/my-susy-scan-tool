if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    sh prepare_prospino.sh tmp${1}
    cd tmp${1}
    cd ../

fi
