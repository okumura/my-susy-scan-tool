if [ $# != 1 ]; then
    echo "$0 <directory name>"
else
    dir=${1}
    rm -rf ${dir}
    mkdir ${dir}
    ln -s SPheno.spc ${dir}/prospino.in.les_houches
    ln -s ../../my-prospino2-codes/Pro2_subroutines/ ${dir}/
fi

