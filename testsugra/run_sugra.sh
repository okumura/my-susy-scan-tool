if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    rm -rf out
    mkdir out
    time python susyscan.py --model 1
    time multinest_marginals.py out/
    mkdir /eos/home-k/ksugizak/www/sugra/sugra${1}
    \cp out/marg.pdf /eos/home-k/ksugizak/www/sugra/sugra${1}/sugra${1}.pdf
    \cp out/marg1d.pdf /eos/home-k/ksugizak/www/sugra/sugra${1}/sugra1d${1}.pdf
    \cp out/params.json out/phys_live.points out/post_equal_weights.dat /eos/home-k/ksugizak/www/sugra/sugra${1}/
    echo "https://cernbox.cern.ch/index.php/apps/files/?dir=/www&"

fi
