import json
import numpy
from numpy import log, exp, pi
import scipy.stats, scipy
import pymultinest
import matplotlib.pyplot as plt
import math
import scipy
from scipy import special
import random
import subprocess
import re
import myparameters as mp
import argparse

def SetFixedValue(cube, name, value):
        if (not (name in mp.parameters)): return
        cube[mp.GetParamIndex(name)] = value
        return

def GaussLogLike(x, mu, sigma, theory_sigma):
        logLike = - math.pow(mu - x, 2) /\
                  (2 * (math.pow(sigma, 2) +
                        math.pow(theory_sigma, 2)))
        return logLike

def GaussLogLikeWithFractionlTheoryError(x, mu, sigma, theory_fractional_sigma):
        logLike = - math.pow(mu - x, 2) /\
                  (2 * (math.pow(sigma, 2) +
                        math.pow(theory_fractional_sigma*x, 2)))
        return logLike

def LowerConstraintLogLike(x, limit):
        logLike = (x < limit) * -1e101
        return logLike

def UpperConstraintLogLike(x, limit):
        logLike = (x > limit) * -1e101
        return logLike

def logPrior(x, minimum, maximum):
        return math.pow(10, math.log10(minimum) + x *
                        (math.log10(maximum) -
                         math.log10(minimum)))

def linearPrior(x, minimum, maximum):
        return minimum + x * (maximum - minimum)

def signParameter(x):
        value = 1
        if x > 0.5: value = -1
        return value

def GaussParameter(x, mu, sigma):
        return mu + sigma * math.sqrt(2) *\
                scipy.special.erfcinv(2 * (1 - x))

def prior(cube, ndim, nparams):
        # MSSM parameters (free independent parameters)
        cube[mp.GetParamIndex('m0')] = logPrior(x=cube[mp.GetParamIndex('m0')], minimum=10000, maximum=40000)
        cube[mp.GetParamIndex('m12')] = logPrior(x=cube[mp.GetParamIndex('m12')], minimum=2500, maximum=5000)
        cube[mp.GetParamIndex('a0')] = linearPrior(x=cube[mp.GetParamIndex('a0')], minimum=-5000, maximum=5000)
        cube[mp.GetParamIndex('tanbeta')] = linearPrior(x=cube[mp.GetParamIndex('tanbeta')], minimum=1, maximum=62)
        cube[mp.GetParamIndex('signmu')] = -1 #signParameter(x=cube[mp.GetParamIndex('signmu')])
        
        # nuisance parameters (SM parameters with constraints)
        # Bottom MS-bar mass.
        # PDG.
        # http://pdg.lbl.gov/2019/listings/rpp2019-list-b-quark.pdf
        cube[mp.GetParamIndex('mb')] = GaussParameter(x=cube[mp.GetParamIndex('mb')], mu=4.18, sigma=0.03)  # up to date (2019.11)
        
        # Top pole mass.
        # PDG.
        # http://pdg.lbl.gov/2019/reviews/rpp2018-rev-standard-model.pdf (10.9)
        cube[mp.GetParamIndex('mt')] = GaussParameter(x=cube[mp.GetParamIndex('mt')], mu=172.74, sigma=(0.33 ** 2 + 0.32 ** 2) ** 0.5)  # up to date (2019.11)
        
        # Strong coupling.
        # PDG
        # http://pdg.lbl.gov/2019/reviews/rpp2018-rev-qcd.pdf (9.23)
        cube[mp.GetParamIndex('alphas')] = GaussParameter(cube[mp.GetParamIndex('alphas')], 0.1181, 0.0011)  # up to date (2019.11)
        
        # Reciprocal of EM coupling at MZ.
        # PDG.
        # http://pdg.lbl.gov/2014/reviews/rpp2014-rev-standard-model.pdf 
        cube[mp.GetParamIndex('invalpha')] = GaussParameter(cube[mp.GetParamIndex('invalpha')], 127.940, 0.014)  # the numbers should be updated (just copy-and-pasted from superpy)
        
        prepSLHA(cube)
        
        # run SPheno
        if mp.doSPheno or mp.doMicrOMEGAs:
                command = ['SPheno', 'LesHouches.in']
                subprocess.run(command)
                results=readSLHA()
        
                if ('mass_spectrum' in results.keys()):
                        if 25 in results['mass_spectrum'].keys(): # mh
                                SetFixedValue(cube, 'mh', results['mass_spectrum'][25])
                        else: SetFixedValue(cube, 'mh', 0)
                        
                        if 24 in results['mass_spectrum'].keys(): # mW
                                SetFixedValue(cube, 'mw', results['mass_spectrum'][24])
                        else: SetFixedValue(cube, 'mw', 0)

                        if 1000001 in results['mass_spectrum'].keys(): # sdown_L mass
                                SetFixedValue(cube, 'msdownL', results['mass_spectrum'][1000001])
                        else: SetFixedValue(cube, 'msdownL', 0)

                        if 2000001 in results['mass_spectrum'].keys(): # sdown_R mass
                                SetFixedValue(cube, 'msdownR', results['mass_spectrum'][2000001])
                        else: SetFixedValue(cube, 'msdownR', 0)

                        if 1000002 in results['mass_spectrum'].keys(): # sup_L mass
                                SetFixedValue(cube, 'msupL', results['mass_spectrum'][1000002])
                        else: SetFixedValue(cube, 'msupL', 0)

                        if 2000002 in results['mass_spectrum'].keys(): # sup_R mass
                                SetFixedValue(cube, 'msupR', results['mass_spectrum'][2000002])
                        else: SetFixedValue(cube, 'msupR', 0)

                        if 1000003 in results['mass_spectrum'].keys(): # sstrange_L mass
                                SetFixedValue(cube, 'msstrangeL', results['mass_spectrum'][1000003])
                        else: SetFixedValue(cube, 'msstrangeL', 0)

                        if 2000003 in results['mass_spectrum'].keys(): # sstrange_R mass
                                SetFixedValue(cube, 'msstrangeR', results['mass_spectrum'][2000003])
                        else: SetFixedValue(cube, 'msstrangeR', 0)

                        if 1000004 in results['mass_spectrum'].keys(): # scharm_L mass
                                SetFixedValue(cube, 'mscharmL', results['mass_spectrum'][1000004])
                        else: SetFixedValue(cube, 'mscharmL', 0)

                        if 2000004 in results['mass_spectrum'].keys(): # scharm_R mass
                                SetFixedValue(cube, 'mscharmR', results['mass_spectrum'][2000004])
                        else: SetFixedValue(cube, 'mscharmR', 0)

                        if 1000005 in results['mass_spectrum'].keys(): # sbottom_1 mass
                                SetFixedValue(cube, 'msbottom1', results['mass_spectrum'][1000005])
                        else: SetFixedValue(cube, 'msbottom1', 0)

                        if 2000005 in results['mass_spectrum'].keys(): # sbottom_2 mass
                                SetFixedValue(cube, 'msbottom2', results['mass_spectrum'][2000005])
                        else: SetFixedValue(cube, 'msbottom2', 0)

                        if 1000006 in results['mass_spectrum'].keys(): # stop_1 mass
                                SetFixedValue(cube, 'mstop1', results['mass_spectrum'][1000006])
                        else: SetFixedValue(cube, 'mstop1', 0)

                        if 2000006 in results['mass_spectrum'].keys(): # stop_2 mass
                                SetFixedValue(cube, 'mstop2', results['mass_spectrum'][2000006])
                        else: SetFixedValue(cube, 'mstop2', 0)

                        if 1000021 in results['mass_spectrum'].keys(): # gluino mass
                                SetFixedValue(cube, 'mgluino', results['mass_spectrum'][1000021])
                        else: SetFixedValue(cube, 'mgluino', 0)
                        
                        if 1000022 in results['mass_spectrum'].keys(): # mchi10 
                                SetFixedValue(cube, 'mchi10', results['mass_spectrum'][1000022])
                        else: SetFixedValue(cube, 'mchi10', 0)
                        
                        if 1000024 in results['mass_spectrum'].keys(): # mchi1+
                                SetFixedValue(cube, 'mchi1+', results['mass_spectrum'][1000024])
                        else: SetFixedValue(cube, 'mchi1+', 0)
                        
                        if mp.IsRegistered('mchi1+') and mp.IsRegistered('mchi10'):
                                SetFixedValue(cube, 'deltam', mp.GetParamFromName(cube, 'mchi1+')-mp.GetParamFromName(cube, 'mchi10'))
                        else:
                                SetFixedValue(cube, 'deltam', 0)

                        if mp.IsRegistered('msdownL') and mp.IsRegistered('msdownR') and mp.IsRegistered('msupL') and mp.IsRegistered('msupR') and mp.IsRegistered('msstrangeL') and mp.IsRegistered('msstrangeR') and mp.IsRegistered('mscharmL') and mp.IsRegistered('mscharmR') and mp.IsRegistered('msbottom1') and mp.IsRegistered('msbottom2') and mp.IsRegistered('mstop1') and mp.IsRegistered('mstop2'):
                                SetFixedValue(cube, 'lightest_squark_mass', min(mp.GetParamFromName(cube, 'msdownL'), mp.GetParamFromName(cube, 'msdownR'), mp.GetParamFromName(cube, 'msupL'), mp.GetParamFromName(cube, 'msupR'), mp.GetParamFromName(cube, 'msstrangeL'), mp.GetParamFromName(cube, 'msstrangeR'), mp.GetParamFromName(cube, 'mscharmL'), mp.GetParamFromName(cube, 'mscharmR'), mp.GetParamFromName(cube, 'msbottom1'), mp.GetParamFromName(cube, 'msbottom2'), mp.GetParamFromName(cube, 'mstop1'), mp.GetParamFromName(cube, 'mstop2')))
                        else:
                                SetFixedValue(cube, 'lightest_squark_mass', 0)

                else:
                        print("WARNING mass spectrum information is not in SPheno.spc - due to some unphysical output. very unlikly parameter will be used on purpose so that such data will not appear in the final list")
                        SetFixedValue(cube, 'mh', 0)
                        SetFixedValue(cube, 'mw', 0)
                        SetFixedValue(cube, 'msdownL', 0)
                        SetFixedValue(cube, 'msdownR', 0)
                        SetFixedValue(cube, 'msupL', 0)
                        SetFixedValue(cube, 'msupR', 0)
                        SetFixedValue(cube, 'msstrangeL', 0)
                        SetFixedValue(cube, 'msstrangeR', 0)
                        SetFixedValue(cube, 'mscharmL', 0)
                        SetFixedValue(cube, 'mscharmR', 0)
                        SetFixedValue(cube, 'msbottom1', 0)
                        SetFixedValue(cube, 'msbottom2', 0)
                        SetFixedValue(cube, 'mstop1', 0)
                        SetFixedValue(cube, 'mstop2', 0)
                        SetFixedValue(cube, 'mgluino', 0)
                        SetFixedValue(cube, 'mchi10', 0)
                        SetFixedValue(cube, 'mchi1+', 0)
                        SetFixedValue(cube, 'deltam', 0)
                        SetFixedValue(cube, 'lightest_squark_mass', 0)
                
                
                if ('low_energy_observables' in results.keys()):
                        if 1 in results['low_energy_observables'].keys(): # BR(b -> s gamma)
                                SetFixedValue(cube, 'bsg', results['low_energy_observables'][1])
                        else: SetFixedValue(cube, 'bsg', 3.54589222E-04)
                        
                        if 2 in results['low_energy_observables'].keys(): # BR(b -> s mu+ mu-)
                                SetFixedValue(cube, 'bsmm', results['low_energy_observables'][2])
                        else: SetFixedValue(cube, 'bsmm', 1.58408218E-06)
                        
                        if 3 in results['low_energy_observables'].keys(): # BR(b -> s nu nu)
                                SetFixedValue(cube, 'bsnn', results['low_energy_observables'][3])
                        else: SetFixedValue(cube, 'bsnn', 3.52426312E-05)
                        
                        if 5 in results['low_energy_observables'].keys(): # BR(Bd -> mu+ mu-)
                                SetFixedValue(cube, 'Bd2mm', results['low_energy_observables'][5])
                        else: SetFixedValue(cube, 'Bd2mm', 9.04162801E-11)
                        
                        if 6 in results['low_energy_observables'].keys(): # BR(Bd -> tau+ tau-)
                                SetFixedValue(cube, 'Bd2tt', results['low_energy_observables'][6])
                        else: SetFixedValue(cube, 'Bd2tt', 1.89282332E-08)
                        
                        if 8 in results['low_energy_observables'].keys(): # BR(Bs -> mu+ mu-)
                                SetFixedValue(cube, 'Bs2mm', results['low_energy_observables'][8])
                        else: SetFixedValue(cube, 'Bs2mm', 3.04600999E-09)
                        
                        if 9 in results['low_energy_observables'].keys(): # BR(Bs -> tau+ tau-)
                                SetFixedValue(cube, 'Bs2tt', results['low_energy_observables'][9])
                        else: SetFixedValue(cube, 'Bs2tt', 6.46096917E-07)
                        
                        if 21 in results['low_energy_observables'].keys(): # Delta(g-2)_muon/2
                                SetFixedValue(cube, 'gm2', results['low_energy_observables'][21])
                        else: SetFixedValue(cube, 'gm2', 6.81179021E-10)
                        
                else:
                        SetFixedValue(cube, 'bsg', 3.54589222E-04)
                        SetFixedValue(cube, 'bsmm', 1.58408218E-06)
                        SetFixedValue(cube, 'bsnn', 3.52426312E-05)
                        SetFixedValue(cube, 'Bd2mm', 9.04162801E-11)
                        SetFixedValue(cube, 'Bd2tt', 1.89282332E-08)
                        SetFixedValue(cube, 'Bs2mm', 3.04600999E-09)
                        SetFixedValue(cube, 'Bs2tt', 6.46096917E-07)
                        SetFixedValue(cube, 'gm2', 6.81179021E-10)                


                if ('nmix' in results.keys()):
                        if '1_1' in results['nmix'].keys(): # n_1_1
                                SetFixedValue(cube, 'N_1_1', results['nmix']['1_1'])
                        else: SetFixedValue(cube, 'N_1_1', 0)

                else:
                        SetFixedValue(cube, 'N_1_1', 0)
                
        # run micrOMEGAs (relic density)
        if mp.doMicrOMEGAs:
                command = ['micromegasMSSM', 'SPheno.spc']
                proc = subprocess.Popen(command, stdout=subprocess.PIPE, universal_newlines=True)
                lines = proc.communicate()[0].split('\n')
                results=analyzeMicrOMEGAsOutput(lines)
                if ('omega' in results.keys()):
                        SetFixedValue(cube, 'relicdensity', results['omega'])
                else:
                        print('WARNING xf is not stored')
                        SetFixedValue(cube, 'relicdensity', 1.0)
        

def loglike(cube, ndim, nparams):
        if mp.flatProbabilityForTesting:
                return log(random.random())
        
        logLike = 0;
        dryRunForDevelopment=True
        
        ####################
        # configuration
        ####################
        if mp.IsRegistered('mw'):
                logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'mw'), 80.379, 0.012, 0.0)
                dryRunForDevelopment=False
                # up to date (2019.10)

        if mp.IsRegistered('mh'):
                logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'mh'), 125.10, 0.14, 0.0) 
                dryRunForDevelopment=False
                # up to date (2019.10)

        if mp.IsRegistered('lightest_squark_mass'):
                logLike = logLike + LowerConstraintLogLike(mp.GetParamFromName(cube, 'lightest_squark_mass'), 1000) 
                dryRunForDevelopment=False
                # up to date (2019.11)

        if mp.IsRegistered('mgluino'):
                logLike = logLike + LowerConstraintLogLike(mp.GetParamFromName(cube, 'mgluino'), 2000) 
                dryRunForDevelopment=False
                # up to date (2019.11)
                
        # if mp.IsRegistered('gm2'): # delta (g-2)/2 for muon
        #         logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'gm2'), 26.8e-10, 6.3e-10, 4.3e-10) 
        #         dryRunForDevelopment=False
        #         # up to date (2019.10)

        if mp.IsRegistered('bsg'):
                logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'bsg'), 3.32e-4, 0.15e-4, 0.23e-4) 
                dryRunForDevelopment=False
                # up to date (2019.12)
                # http://pdg.lbl.gov/2019/reviews/rpp2018-rev-b-meson-prod-decay.pdf

        # if mp.IsRegistered('bsmm'):
        #         logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'bsmm'), 2.23e-6, 0.98e-6, 0.11e-6) 
        #         dryRunForDevelopment=False
        #         # the numbers should be updated (just copy-and-pasted from superpy)
                                
        # if mp.IsRegistered('bsnn'):
        #         logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'bsnn'), 1.14e-4, 0.27e-4, 0.38e-4) 
        #         dryRunForDevelopment=False
        #         # the numbers should be updated (just copy-and-pasted from superpy)

        if mp.IsRegistered('Bs2mm'):
                logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'Bs2mm'), 2.9e-9, 0.7e-9, 0.2e-9) 
                dryRunForDevelopment=False
                # up to date (2019.12)
                # http://cds.cern.ch/record/2684828

        if mp.IsRegistered('N_1_1'):
                logLike = logLike + UpperConstraintLogLike(abs(mp.GetParamFromName(cube, 'N_1_1')), 0.94) 
                dryRunForDevelopment=False
                # up to date (2019.10)
                
        # if mp.IsRegistered('relicdensity'):
        #         logLike = logLike + GaussLogLike(mp.GetParamFromName(cube, 'relicdensity'), 0.1200, 0.0012, 0.0)  
        #         dryRunForDevelopment=False
        #         # https://arxiv.org/pdf/1807.06209.pdf (23)
        #         # up to date (2019.11)
                
        if dryRunForDevelopment:
                return log(random.random())
        else:   return logLike
                
        

def prepSLHA(cube, MZ=9.11876000e+01):
        f = open('LesHouches.in', 'w')
        print ('Block MODSEL                    # Select model', file=f)
        print (' 1   %d                         # sugra'%(mp.SUSYModel), file=f)
        print ('Block SMINPUTS                  # Standard Model inputs', file=f)
        print (' 1   %s                         # alpha^(-1) SM MSbar(MZ)'%(mp.GetParamFromName(cube, 'invalpha')), file=f)
        print (' 2   1.16637000e-05             # G_Fermi', file=f)
        print (' 3   %s                         # alpha_s(MZ) SM MSbar'%(mp.GetParamFromName(cube, 'alphas')), file=f)
        print (' 4   %s                         # MZ(pole)'%(str(MZ)), file=f)
        print (' 5   %s                         # mb(mb) SM MSbar'%(mp.GetParamFromName(cube, 'mb')), file=f)
        print (' 6   %s                         # mtop(pole)'%(mp.GetParamFromName(cube, 'mt')), file=f)
        print (' 7   1.77700000e+00             # mtau(pole)', file=f)
        print ('Block MINPAR    # Input parameters', file=f)
        print (' 1   %s                         # m0'%(mp.GetParamFromName(cube, 'm0')), file=f)
        print (' 2   %s                         # m12'%(mp.GetParamFromName(cube, 'm12')), file=f)
        print (' 3   %s                         # tanb'%(mp.GetParamFromName(cube, 'tanbeta')), file=f)
        print (' 4   %s                         # sign(mu)'%(mp.GetParamFromName(cube, 'signmu')), file=f)
        print (' 5   %s                         # A0'%(mp.GetParamFromName(cube, 'a0')), file=f)
        print ('Block SPhenoInput       # SPheno specific input', file=f)
        print (' 1  -1                  # error level', file=f)
        print (' 2   1                  # SPA conventions', file=f)
        print ('11   1                  # calculate branching ratios', file=f)
        print ('12   1.00000000E-04     # write only branching ratios larger than this value', file=f)
        print ('21   0                  # calculate cross section', file=f)

def analyzeMicrOMEGAsOutput(lines):
        results={}
        for line in lines:
                if line.startswith('Xf'):
                        components=line.split(' ')
                        if len(components)!=2:
                                print('PLEASE check the micromegas output format', line)
                        xf=float(components[0].split('=')[1])
                        omega=float(components[1].split('=')[1])
                        results.update({'xf':xf}) # Xf = Mcdm/Tf characterizes the freeze-out temperature
                        results.update({'omega':omega}) # the dark matter relic density
        return results

def readSLHA():
        f = open('SPheno.spc')
        lines = f.readlines()
        results={}
        
        iline=0
        while iline < len(lines):
                line=lines[iline]
                line=line.replace('\n', '')
                iline=iline+1 # global pointer increment
                kline=iline   # local pointer initialize
                if line.startswith('Block') or line.startswith('DECAY'):
                        
                        if line.startswith('Block MASS'): # Block MASS
                                mass_spectrum={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        #print(line) # for debugging
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)
                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) != 2: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                        mass_spectrum.update({int(info[0]):float(info[1])}) # PDG code ": mass
                                results.update({'mass_spectrum':mass_spectrum})
                        
                                
                        elif line.startswith('Block SPhenoLowEnergy'): # Block SPhenoLowEnergy
                                low_energy_observables={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        #print(line) # for debugging
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)
                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) != 2: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                        low_energy_observables.update({int(info[0]):float(info[1])}) # observable ID ": observable
                                results.update({'low_energy_observables':low_energy_observables})
                                
                                
                        elif line.startswith('DECAY'): # DECAY
                                decay_modes=[]
                                info=[]
                                components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                for component in components:
                                        if component=='': continue
                                        if component=='#': break
                                        info.append(component)
                                if len(info) == 0: continue; # a line with only comments
                                if len(info) != 3: 
                                        print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                particle_decay_info={'PDG':info[1], 'WIDTH':float(info[2])}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        daughters=[]
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        #print(line) # for debugging
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)
                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 2: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                        for ii in range(2, len(info)):
                                                daughters.append(int(info[ii]))
                                        if int(info[1])!=len(daughters):
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line) # info[1] is supposed to be NDA = Number of daughters
                                        decay_mode={'BR':float(info[0]), 'DAUGHTERS':daughters}
                                        decay_modes.append(decay_mode)
                                particle_decay_info.update({'MODES':decay_modes})
                                if (not 'decays' in results.keys()):
                                        results.update({'decays':[]})
                                results['decays'].append(particle_decay_info)


                        elif line.startswith('Block Nmix'): # neutralino mixing
                                nmix={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)

                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 3: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                                continue
                                        key='%d_%d'%(int(info[0]), int(int(info[1])))
                                        nmix[key]=float(info[2])
                                        
                                results.update({'nmix':nmix})

                        elif line.startswith('Block Umix'): # chargino mixing
                                umix={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)

                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 3: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                                continue
                                        key='%d_%d'%(int(info[0]), int(int(info[1])))
                                        umix[key]=float(info[2])
                                        
                                results.update({'umix':umix})

                        elif line.startswith('Block Vmix'): # chargino mixing
                                vmix={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)

                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 3: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                                continue
                                        key='%d_%d'%(int(info[0]), int(int(info[1])))
                                        vmix[key]=float(info[2])
                                        
                                results.update({'vmix':vmix})

                        elif line.startswith('Block Hmix'): # chargino mixing
                                hmix={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)

                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 2: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                                continue
                                        key='%d'%(int(info[0]))
                                        hmix[key]=float(info[1])
                                        
                                results.update({'hmix':hmix})

                        elif line.startswith('Block MSOFT'): # chargino mixing
                                msoft={}
                                while (kline < len(lines)) and (not lines[kline].startswith('Block')) and (not lines[kline].startswith('DECAY')):
                                        line=lines[kline]
                                        line=line.replace('\n', '')
                                        kline=kline+1
                                        components=re.split(' +', line) # split a line into components with space (multiple spaces)
                                        
                                        info=[]
                                        for component in components:
                                                if component=='': continue
                                                if component=='#': break
                                                info.append(component)

                                        if len(info) == 0: continue; # a line with only comments
                                        if len(info) < 2: 
                                                print('WARNING: PLEASE CHECK SLHA FORMAT:', line)
                                                continue
                                        key='%d'%(int(info[0]))
                                        msoft[key]=float(info[1])
                                        
                                results.update({'msoft':msoft})


        return results


def main(): # executable
        parser = argparse.ArgumentParser('usage:')
        parser.add_argument('--model', action='store', type=int, default=-1)
        arguments = parser.parse_args()
        if arguments.model==-1:
                print('please specify the SUSY-breaking model (with --model option): 1=mSugra 2=GMSB 3=AMSB')
        else:
                mp.SUSYModel=arguments.model
                pymultinest.run(loglike, prior, mp.n_params, outputfiles_basename='out/',
                                resume = False, verbose = True)
                json.dump(mp.parameters, open('out/params.json', 'w')) # save parameter names
        

if __name__ == '__main__':
        main()
        
