#!/bin/bash

id=${1}

echo ${id}
cd /data/data10/zp/ksugizak/workspace/my-susy-scan-tool/ # if necessary change to proper directory
source setup.sh
cd testsugra_NUHM/tmp${1}
python ../analysis.py --params ../out/params.json --points ../out/.txt --output outtmp --ndivs 250 --nth ${1}
cd ../

