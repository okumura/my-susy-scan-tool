if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    cd ..; mv testsugra_NUHM${1} /data/data10/zp/ksugizak/workspace/my-susy-scan-tool/
    echo "Moved testsugra_NUHM${1} from /afs to /data/data10."

fi
