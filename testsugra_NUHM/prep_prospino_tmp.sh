if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    for i in `seq 1 ${1}`
    do
	dir=tmp$i
	rm -rf ${dir}
	mkdir ${dir}
	ln -s SPheno.spc ${dir}/prospino.in.les_houches
	ln -s ../../my-prospino2-codes/Pro2_subroutines/ ${dir}/
	#sh prepare_prospino.sh tmp$i
	#cd tmp$i
	#cd ../
    done
fi
