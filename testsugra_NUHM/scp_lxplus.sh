if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    ssh lxplus "mkdir -p /eos/home-k/ksugizak/www/sugraNUHM/sugraN${1}"
    scp out/marg.pdf lxplus:/eos/home-k/ksugizak/www/sugra/sugraNUHM${1}/sugraN${1}.pdf
    scp out/params.json out/phys_live.points out/post_equal_weights.dat out/.txt lxplus:/eos/home-k/ksugizak/www/sugraNUHM/sugraN${1}/
    cd ..; scp -r testsugra_NUHM${1} lxplus:/afs/cern.ch/work/k/ksugizak/public/workspace/my-susy-scan-tool/
    echo "https://cernbox.cern.ch/index.php/apps/files/?dir=/www&"

fi
