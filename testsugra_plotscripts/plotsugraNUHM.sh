if [ $# -ne 1 ]; then
    echo "$# options are detected."
    echo "usage: $0 <number>" 
else
    root -l -b -q 'plot.C("output.root")'
    \cp -f output.pdf /eos/home-k/ksugizak/www/sugraNUHM/sugraN${1}/sugraN${1}root.pdf
fi
